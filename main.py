import math
import numpy as np
import matplotlib.pyplot as plt
from numpy import genfromtxt


# //////////////////////////
# the cost function
def compute_cost(x,y,w,b):
    m=x.shape[0]
    cost_sum=0
    for i in range(m):
       fx=w*x[i]+b
       cost=(fx-y[i])**2
       cost_sum=cost_sum+cost
    return cost_sum/2*m
# //////////////////////////
# ///////////////////////////
# present data with model function
def represent_data(x_train, y_train,fx):
    plt.scatter(x_train, y_train, marker='x', c='r',label='actual valus')
    plt.plot(x_train, fx, c='b', label='our prediction')
    plt.title('Housing Prices')
    plt.ylabel('Price (in 1000s of dollars)')
    plt.xlabel('Size (1000 sqft)')
    plt.legend()
    plt.show()
# ///////////////////////////
# ////////////////////////////
# compute model output function
def compute_model_output(x,w,b):
    m=x.shape[0]
    f_wb=np.zeros(m)
    for  i in range(m):
        f_wb[i]=w*x[i]+b
    return f_wb
# /////////////////////////////
# compute the derivative term in the gradient descent algorithm
def compute_derivative(x,y,w,b):
    m=x.shape[0]
    dj_dw=0
    dj_db=0
    for i in range(m):
        fx=w*x[i]+b
        dj_dw_i=(fx-y[i])*x[i]
        dj_db_i=fx-y[i]
        dj_dw+=dj_dw_i
        dj_db+=dj_db_i
    dj_dw=dj_dw/m
    dj_db=dj_db/m
    return dj_dw,dj_db
# /////////////////////////////////
# compute gradient descent algorithm
def gradient_descent(x,y,w_in,b_in,alpha,iterations,cost_function,derivative_function):
    # save valuse of cost function to plot them later
    j_history=[]
    p_history=[]
    w=w_in
    b=b_in
    for i in range(iterations):
        dj_dw,dj_db=derivative_function(x,y,w,b)
        b=b-alpha*dj_db
        w=w-alpha*dj_dw
        if i<100000:
            j_history.append(cost_function(x,y,w,b))
            p_history.append([w,b])
        # Print cost every at intervals 10 times or as many iterations if < 10
        if i % math.ceil(iterations / 10) == 0:
            print(f"Iteration {i:4}: Cost {j_history[-1]:0.2e} ",
                  f"dj_dw: {dj_dw: 0.3e}, dj_db: {dj_db: 0.3e}  ",
                  f"w: {w: 0.3e}, b:{b: 0.5e}")
    return w,b,j_history,p_history
# ////////////////////////////////////
# plot the cost function to number of iterations
def plot_cost(j_history):
    fig, (ax1, ax2) = plt.subplots(1, 2, constrained_layout=True, figsize=(12, 4))
    ax1.plot(j_history[:100])
    ax2.plot(100 + np.arange(len(j_history[100:])), j_history[100:])
    ax1.set_title("Cost vs. iteration(start)");
    ax2.set_title("Cost vs. iteration (end)")
    ax1.set_ylabel('Cost');
    ax2.set_ylabel('Cost')
    ax1.set_xlabel('iteration step');
    ax2.set_xlabel('iteration step')
    plt.show()
# //////////////////////////////////////
# x_train=np.array([1.0,2.0])
# y_train=np.array([300.0,500.0])
data = np.genfromtxt("data1.csv", delimiter=',')
x_train=data[:,0]
y_train=data[:,1]
m=x_train.shape[0]
# ///////get familiar with the data
print(f"x_tarin={x_train}")
print((f"y_train={y_train}"))
print(f"number of training examples= {m}")
# /////intialize parameters
w=0
b=0
# alpha=0.02e-2
alpha=0.02
iterations=1000
final_w,final_b,j_history,p_history=gradient_descent(x_train,y_train,w,b,alpha,iterations,compute_cost,compute_derivative)
print(f"final w = {final_w}")
print(f"final b = {final_b}")
# print(f"this is pramaters{p_history}")
# //////make a prediction
print(f"1000 sqft house prediction {final_w*300 + final_b} Thousand dollars")
# //////plot the cost to iteration
plot_cost(j_history)
# //////present data and the model
fx=compute_model_output(x_train,final_w,final_b)
print(f"the model output is = {fx}")
represent_data(x_train,y_train,fx)

